import React from 'react';

const Info = ({info}) => {

    if(Object.keys(info).length === 0) return null;

    const { strGenre, strArtistThumb, strWebsite, strBiographyEN, strCountry, strArtist } = info;
 
    return (
        <div className="col-12 col-md-6">
            <h2 className="text w-100 text-center">Band Biography</h2>
            <img src={strArtistThumb} alt="artista" className="w-100 mb-3" />
            <h4 className="text text-bold">{strArtist}</h4>
            <p className="text text-bold">From: {strCountry}</p>
            <p className="text text-bold">Genere: {strGenre}</p>
            <p className="text text-bold">Site: <a href={strWebsite}>{strWebsite}</a></p>
            <div className="card p-2">
                <p className="text">{strBiographyEN}</p>
            </div>
        </div>
    );
}

export default Info;