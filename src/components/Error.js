import React from 'react';

const Error = ({message}) => {
    return (
        <div className="alert alert-danger d-block text-center" role="alert">
            <p className="text mb-0">{message}</p>
        </div>
    );
}

export default Error;