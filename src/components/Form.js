import React, { useState } from 'react';
import Error from './Error';

const Form = ({saveSearchLyric}) => {

    // initial state
    const [ search, saveSearch ] = useState({
        artist: '',
        song: ''
    });

    // get artist and song
    const { artist, song } = search;

    // error state
    const [ error, saveError ] = useState(false);

    // check inputs and read content
    const handleState = e =>  {
        saveSearch({
            ...search,
            [e.target.name] : e.target.value
        })
    }

    // check APIs
    const checkData = e =>  {
        e.preventDefault();

        if(artist.trim() === '' || song.trim() === '') {
            saveError(true);
            return;
        }

        // step-front to error and check API
        saveError(false);

        saveSearchLyric(search);
    }   


    return (
        <form
            className="w-100 card p-3 bg-dark"
            onSubmit={checkData}
        >
            { error ? <Error message="Complete fields" /> : null }

            <div className="form-group">
                <label htmlFor="" className="text text-white">Band</label>
                <input
                    placeholder="Band"
                    className="form-control"
                    name="artist"
                    onChange={handleState}
                    value={artist}
                    type="text"/>
            </div>

            <div className="form-group mb-4">
                <label htmlFor="" className="text text-white">Song</label>
                <input
                    placeholder="Lyrics"
                    className="form-control"
                    name="song"
                    onChange={handleState}
                    value={song}
                    type="text"/>
            </div>
            <div className="form-group mb-0">
                <input
                    value="Send"
                    className="form-control btn btn-success"
                    type="submit"/>
            </div>
        </form>

        
    );
}

export default Form;