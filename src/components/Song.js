import React from 'react';

const Song = ({lyric}) => {

    if(lyric.length === 0) return null;
 
    return (
        <div className="col-12 col-md-6">
            <h2 className="text w-100 text-center">Song Lyric</h2>
            <p className="text">{lyric}</p>
        </div>
    );
}

export default Song;