import React, {useEffect, useState } from 'react';
import axios from 'axios';

import Form from './components/Form';
import Song from './components/Song';
import Info from './components/Info';

function App() {

  // define state
  const [ searlyric, saveSearchLyric ] = useState({});

  // define lyrics on state
  const [ lyric, saveLyric ] = useState([]);

  // define artist on state
  const [ info, saveInfo ] = useState([]);

  // chech API
  useEffect(() => {

    // check songs
    if(Object.keys(searlyric).length === 0 ) return;

    const checkApiLyrics = async () => {
      const { artist, song } = searlyric;
      const url = `https://api.lyrics.ovh/v1/${artist}/${song}`;
      const url2 = `https://www.theaudiodb.com/api/v1/json/1/search.php?s=${artist}`;

      // check bouth APIs
      const [ lyric, info ] = await Promise.all([
        axios(url),
        axios(url2)
      ]);

      saveLyric(lyric.data.lyrics);
      saveInfo(info.data.artists[0]);

      // saveLyric(result.data.lyrics);
    }

    checkApiLyrics();

  }, [searlyric, info]);

  return (
    <div className="container mt-5">
      <div className="row">
        <div className="col-12 text-center mb-3">
          <h2 className="text text-size-3">Bands / Lyrics</h2>
        </div>

        <div className="col-12 col-md-6 mx-auto mb-5">
          <Form
            saveSearchLyric={saveSearchLyric}
          />
        </div>

        <div className="col-12 d-flex flex-wrap">
          <Info
            info={info}
          />
          <Song
            lyric={lyric}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
